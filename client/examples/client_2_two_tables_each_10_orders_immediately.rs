use futures::SinkExt;
use futures::StreamExt;
use futures::TryStreamExt;
use protocol::codec::ClientCodec;
use protocol::*;
use tokio::net::TcpStream;
use tokio::sync::mpsc::unbounded_channel;
use tokio_util::codec::Framed;
use tracing::*;

#[tokio::main]
async fn run() -> Result<(), anyhow::Error> {
    // Connect to a peer
    let stream = TcpStream::connect("127.0.0.1:8080").await?;
    info!("bind to {:?}", stream.local_addr());
    let mut lines = Framed::new(stream, ClientCodec);

    let (tx, mut rx) = unbounded_channel::<Request>();

    let tx_1 = tx.clone();
    let async_1 = async move {
        let ret: Result<Vec<_>, anyhow::Error> = (0..10)
            .into_iter()
            .map(|i| {
                let req = Request::Order(Table(2), vec![Item(i)]);
                (tx_1.clone(), req)
            })
            .map(move |(tx, inner)| async move {
                tx.send(inner)?;
                Ok(())
            })
            .collect::<futures::stream::FuturesOrdered<_>>()
            .try_collect()
            .await;
        ret
    };

    let tx_2 = tx.clone();
    let async_2 = async move {
        let ret: Result<Vec<_>, anyhow::Error> = (0..10)
            .into_iter()
            .map(|i| {
                let msg = Request::Order(Table(3), vec![Item(i)]);
                (tx_2.clone(), msg)
            })
            .map(move |(tx, inner)| async move {
                tx.send(inner)?;
                Ok(())
            })
            .collect::<futures::stream::FuturesOrdered<_>>()
            .try_collect()
            .await;
        ret
    };

    // count down
    for i in 0..3 {
        info!("count down ... {}", 3 - i);
        tokio::time::sleep(std::time::Duration::new(1, 0)).await;
    }

    // listen
    let r = tokio::spawn(async move {
        loop {
            tokio::select! {
                r1 = lines.next() => {
                    match r1 {
                        Some(msg) => {
                            info!(?msg);
                        }
                        None => {
                            info!("server disconnected");
                            break;
                        }
                    }
                }

                msg=rx.recv() => {
                    if let Some(msg) = msg{
                        lines.send(msg).await.unwrap();
                    }
                }
            }
        }
    });

    let _ = tokio::join!(async_1, async_2);

    r.await?;

    Ok(())
}

fn main() -> Result<(), anyhow::Error> {
    tracing_subscriber::fmt()
        .with_timer(protocol::time::JST)
        .init();

    run()
}
