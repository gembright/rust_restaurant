use futures::SinkExt;
use futures::StreamExt;
use protocol::codec::ClientCodec;
use protocol::*;
use tokio::net::TcpStream;
use tokio::sync::mpsc::unbounded_channel;
use tokio_util::codec::Framed;
use tracing::*;

#[tokio::main]
async fn run() -> Result<(), anyhow::Error> {
    // Connect to a peer
    let stream = TcpStream::connect("127.0.0.1:8080").await?;
    info!("bind to {:?}", stream.local_addr());
    let mut lines = Framed::new(stream, ClientCodec);

    let (tx, mut rx) = unbounded_channel::<Request>();

    let tx_1 = tx.clone();
    let async_1 = async move {
        for i in (0..10).into_iter() {
            tokio::time::sleep(std::time::Duration::new(1, 0)).await;
            let req = Request::Order(Table(0), vec![Item(i)]);
            tx_1.send(req).map_err(anyhow::Error::msg)?;
        }
        Result::<(), anyhow::Error>::Ok(())
    };

    let tx_2 = tx.clone();
    let async_2 = async move {
        for i in (0..10).into_iter() {
            tokio::time::sleep(std::time::Duration::new(1, 0)).await;
            let msg = Request::Order(Table(1), vec![Item(i)]);
            tx_2.send(msg).map_err(anyhow::Error::msg)?;
        }
        Result::<(), anyhow::Error>::Ok(())
    };

    let r = tokio::spawn(async move {
        loop {
            tokio::select! {
                r1 = lines.next() => {
                    match r1 {
                        Some(server_msg) => {
                            //let response: Response = serde_json::from_str(&msg).unwrap();
                            info!(?server_msg)
                        }
                        None => {
                            info!("server disconnected");
                            break;
                        }
                    }
                }

                msg=rx.recv() => {
                    //info!("send to server, {:?}", msg);
                    if let Some(msg) = msg{
                        lines.send(msg).await.unwrap();
                    }
                }
            }
        }
    });

    for i in 0..3 {
        info!("count down ... {}", 3 - i);
        tokio::time::sleep(std::time::Duration::new(1, 0)).await;
    }

    let _ = tokio::join!(async_1, async_2);

    r.await?;

    Ok(())
}

fn main() -> Result<(), anyhow::Error> {
    tracing_subscriber::fmt()
        .with_timer(protocol::time::JST)
        .init();

    run()
}
