## client_1_two_tables_each_10_orders_one_by_one
Table `0` and Table `1` run concurrently to order 10 items one at a time and 1 sec delay before next item)
```bash
cargo run --release --example client_1_two_tables_each_10_orders_one_by_one
```
## flamegraph
![client_1_flamegraph.svg](client_1_flamegraph.svg)


## client_2_two_tables_each_10_orders_immediately
Table `2` and Table `3` run concurrently to order 10 items immediately
```bash
cargo run --release --example client_2_two_tables_each_10_orders_immediately
```
## flamgegraph
![client_2_flamegraph.svg](client_2_flamegraph.svg)

