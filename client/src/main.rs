use futures::SinkExt;
use futures::StreamExt;
use protocol::codec::ClientCodec;
use protocol::*;
use std::io::Write;
use std::process::Command;
use tokio::net::TcpStream;
use tokio::sync::mpsc::unbounded_channel;
use tokio_util::codec::Framed;
use tracing::*;

const KEEP_ALIVE: u64 = 30;

#[tokio::main]
async fn run() -> Result<(), anyhow::Error> {
    // Connect to a peer
    let stream = TcpStream::connect("127.0.0.1:8080").await?;
    let addr = stream.local_addr()?;
    info!("bind to {:?}", stream.local_addr());

    let mut lines = Framed::new(stream, ClientCodec);
    // wait for `welcome` message
    info!(
        "{:?}",
        lines
            .next()
            .await
            .ok_or_else(|| anyhow::Error::msg("server disconnected"))?
    );

    let (tx, mut rx) = unbounded_channel::<Request>();

    tokio::spawn(async move {
        loop {
            tokio::select! {
                // receive response from server
                r1 = lines.next() => {
                    match r1 {
                        Some(msg) => {
                            info!("{:#?}", msg);
                        }
                        None => {
                            info!("server disconnected");
                            break;
                        }
                    }
                }

                // send request to server
                msg = rx.recv() => {
                    if let Some(msg) = msg{
                        lines.send(msg).await.unwrap();
                    }
                }

                // keep alive
                _ = tokio::time::sleep(tokio::time::Duration::from_secs(KEEP_ALIVE)) => {
                    lines.send(Request::Ping).await.unwrap();
                }
            }
        }
    });

    // start console loop
    loop {
        let mut cmd = String::new();

        print!("{} > ", addr);
        let _ = std::io::stdout().flush();
        if std::io::stdin().read_line(&mut cmd).is_err() {
            info!("error");
            continue;
        }
        let cmd = cmd.trim();
        if cmd.is_empty() {
            continue;
        }

        let v = cmd.split(' ').collect::<Vec<_>>();
        match v[..] {
            ["table" | "tb" | "t", tb_no] if tb_no.parse::<usize>().is_ok() => {
                let table = tb_no.parse::<usize>()?;
                tx.send(Request::SnapshotAll(Table(table)))?;
            }

            ["table" | "tb" | "t", tb_no, "item" | "i", item_no]
                if matches!(
                    (tb_no.parse::<usize>(), item_no.parse::<usize>()),
                    (Ok(_), Ok(_))
                ) =>
            {
                let table = tb_no.parse::<usize>()?;
                let item = item_no.parse::<usize>()?;
                tx.send(Request::Snapshot(Table(table), Item(item)))?;
            }

            ["order" | "o", "table" | "tb" | "t", tb_no, "item" | "i", item_no]
                if matches!(
                    (tb_no.parse::<usize>(), item_no.parse::<usize>()),
                    (Ok(_), Ok(_))
                ) =>
            {
                let table = tb_no.parse::<usize>()?;
                let item = item_no.parse::<usize>()?;
                tx.send(Request::Order(Table(table), vec![Item(item)]))?;
            }

            ["delete" | "d", "table" | "tb" | "t", tb_no, "item" | "i", item_no]
                if matches!(
                    (tb_no.parse::<usize>(), item_no.parse::<usize>()),
                    (Ok(_), Ok(_))
                ) =>
            {
                let table = tb_no.parse::<usize>()?;
                let item = item_no.parse::<usize>()?;
                tx.send(Request::Delete(Table(table), Item(item)))?;
            }

            ["clear" | "c"] => {
                Command::new("clear")
                    .stdin(std::process::Stdio::null())
                    .spawn()
                    .expect("clear command failed to start");
            }

            ["usage" | "help" | "h"] => {
                info!(
                    "USAGE: 
                            > [table|tb|t] 1                        #snapshot all items for table 1
                            > [table|tb|t] 1 [item|i] 2             #snapshot item 2 for table 1
                            > [order|o] [table|tb|t] 1 [item|i] 2   #order item 2 for table 1
                            > [delete|d] [table|tb|t] 1 [item|i] 2  #delete item 2 for table 1
                            > [exit|x]                              #exit terminal
                            > [usage|help|h]                        #call help
                    "
                );
            }
            ["exit" | "x"] => {
                break;
            }

            _ => {
                error!("Invalid parameters, err= {:?}", v);
            }
        }
    }

    Ok(())
}

fn main() -> Result<(), anyhow::Error> {
    tracing_subscriber::fmt()
        .with_timer(protocol::time::JST)
        .init();

    run()
}
