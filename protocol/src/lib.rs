use byteorder::BigEndian;
use byteorder::ByteOrder;
use bytes::BufMut;
use bytes::BytesMut;
use tokio_util::codec::Decoder;
use tokio_util::codec::Encoder;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, serde::Serialize, serde::Deserialize)]
pub struct Item(pub usize); // item on the menu, use usize as suggested by guidline to simplfy design
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, serde::Serialize, serde::Deserialize)]
pub struct Table(pub usize); // Table, use usize as suggested by guidline to simplfy design

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum Request {
    Ping,
    Order(Table, Vec<Item>), // Requirement: The application MUST, upon creation request, store the item, the table number, and how long the item will take to cook.
    Delete(Table, Item), // Requirement: The application MUST, upon deletion request, remove a specified item for a specified table number.
    SnapshotAll(Table), // Requirement: The application MUST, upon query request, show all items for a specified table number.
    Snapshot(Table, Item), // Requirement: The application MUST, upon query request, show a specified item for a specified table number.
}

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
pub enum OrderStatus {
    Deleted {
        time: chrono::DateTime<chrono::offset::Local>,
    },
    Delivered {
        time: chrono::DateTime<chrono::offset::Local>,
    },
    CantDeliver {
        time: chrono::DateTime<chrono::offset::Local>,
    },
    OnCooking,
}

#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
pub struct OrderInfo {
    pub table: Table,
    pub item: Item,
    pub received_time: chrono::DateTime<chrono::offset::Local>,
    pub cook_time: std::time::Duration,
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub enum Response {
    Confirmation(Vec<Result<OrderInfo, String>>),
    Deleted {
        time: chrono::DateTime<chrono::offset::Local>,
        order_info: OrderInfo,
    },
    Delivery {
        time: chrono::DateTime<chrono::offset::Local>,
        order_info: OrderInfo,
    },
    NoSuchItemToDelete(Item),
    SnapshotAll(Vec<(OrderStatus, OrderInfo)>),
    Snapshot(Vec<(OrderStatus, OrderInfo)>),
    Welcome {
        table_size: usize,
    },
}

pub mod time {
    use time::macros::offset;
    use tracing_subscriber::fmt::format::Writer;
    use tracing_subscriber::fmt::time::FormatTime;
    pub struct JST;

    impl FormatTime for JST {
        fn format_time(&self, w: &mut Writer<'_>) -> std::fmt::Result {
            let r = time::OffsetDateTime::now_utc().to_offset(offset!(+9));
            write!(w, "{} {}", r.date(), r.time())
        }
    }
}

pub mod codec {
    use super::*;
    pub struct ClientCodec;
    pub struct ServerCodec;

    impl Encoder<Request> for ClientCodec {
        type Error = anyhow::Error;

        fn encode(&mut self, msg: Request, dst: &mut BytesMut) -> Result<(), Self::Error> {
            let msg = serde_json::to_string(&msg).unwrap();
            let msg_ref: &[u8] = msg.as_ref();

            dst.reserve(msg_ref.len() + 2);
            dst.put_u16(msg_ref.len() as u16);
            dst.put(msg_ref);

            Ok(())
        }
    }

    impl Decoder for ClientCodec {
        type Item = Response;
        type Error = anyhow::Error;

        fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
            let size = {
                if src.len() < 2 {
                    return Ok(None);
                }
                BigEndian::read_u16(src.as_ref()) as usize
            };

            if src.len() >= size + 2 {
                let _ = src.split_to(2);
                let buf = src.split_to(size);
                Ok(Some(serde_json::from_slice::<Response>(&buf)?))
            } else {
                Ok(None)
            }
        }
    }

    impl Encoder<Response> for ServerCodec {
        type Error = anyhow::Error;

        fn encode(&mut self, msg: Response, dst: &mut BytesMut) -> Result<(), Self::Error> {
            let msg = serde_json::to_string(&msg).unwrap();
            let msg_ref: &[u8] = msg.as_ref();

            dst.reserve(msg_ref.len() + 2);
            dst.put_u16(msg_ref.len() as u16);
            dst.put(msg_ref);
            Ok(())
        }
    }

    impl Decoder for ServerCodec {
        type Item = Request;
        type Error = anyhow::Error;

        fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
            let size = {
                if src.len() < 2 {
                    return Ok(None);
                }
                BigEndian::read_u16(src.as_ref()) as usize
            };

            if src.len() >= size + 2 {
                let _ = src.split_to(2);
                let buf = src.split_to(size);
                Ok(Some(serde_json::from_slice::<Request>(&buf)?))
            } else {
                Ok(None)
            }
        }
    }
}
