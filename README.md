# rust_restaurant
A client-server implementation to simulate kitchen(server) and staff(client)

# Tech Stack
    rust lang + tcp + tokio + tracing + serde + json + anyhow + chrono 

## Download
```
git clone https://gitlab.com/gembright/rust_restaurant.git
```
## Compile
rust lang has to get installed before compiling
#### rust lang installation
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
#### compile
```bash
cd ./rust_restaurant/
./cargo buid --release --tests --examples
```

## Server
A [server](./server) simulates restaurant to take orders from staffs
```bash
cargo run --release --bin server
```
![image](/uploads/661809fb5703eb44568ff668528497a6/image.png)

##### assumations(server)
1. Retaurant has `1000` table sub-services. Each table service is independent and run concurrently.
2. Currently it simply listens to `127.0.0.1:8080`
3. Currently server `disconnect` if client didn't send any request in 60 seconds

## Client
A [client](./client) simulates staff to order, snapshot and delete for a specified table
1. Note that it's an ***interactive*** client, type `h` after running client
2. Note that we can run multiple clients or examples in the same time
```bash
cargo run --release --bin client
```
#### Client usage
![image](/uploads/e76ffb5c9bad5a5007a8fa88e34d9915/image.png)

##### assumations(client)
1. The table number ranges from `0` - `999` or request failed because out-of-boundry 
2. For now, it simply connects to `127.0.0.1:8080`
3. A server must run before client or client failed to connect

#### Examples
The `client` provides `examples` to run some concurrent tests, See [examples](./client/examples)


## Protocol
The [protocol](./protocol) is a library to define how client and server communicate to each other.
Here I implement my `codec` to have serialization and de-serializeation in convenient way 

#### protocol::Request
client sends a `request` to server
```rust
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub enum Request {
    Ping,
    Order(Table, Vec<Item>), // Requirement: The application MUST, upon creation request, store the item, the table number, and how long the item will take to cook.
    Delete(Table, Item), // Requirement: The application MUST, upon deletion request, remove a specified item for a specified table number.
    SnapshotAll(Table), // Requirement: The application MUST, upon query request, show all items for a specified table number.
    Snapshot(Table, Item), // Requirement: The application MUST, upon query request, show a specified item for a specified table number.
}
```
#### protocol::Response
Server sends `response` to client
```rust
#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub enum Response {
    Confirmation(Vec<Result<OrderInfo, String>>),
    Deleted {
        time: chrono::DateTime<chrono::offset::Local>,
        order_info: OrderInfo,
    },
    Delivery {
        time: chrono::DateTime<chrono::offset::Local>,
        order_info: OrderInfo,
    },
    NoSuchItemToDelete(Item),
    SnapshotAll(Vec<(OrderStatus, OrderInfo)>),
    Snapshot(Vec<(OrderStatus, OrderInfo)>),
    Welcome {
        table_size: usize,
    },
}
```

#### protocol::codec
```rust
    impl Encoder<Request> for ClientCodec {
        type Error = anyhow::Error;

        fn encode(&mut self, msg: Request, dst: &mut BytesMut) -> Result<(), Self::Error> {
            let msg = serde_json::to_string(&msg).unwrap();
            let msg_ref: &[u8] = msg.as_ref();

            dst.reserve(msg_ref.len() + 2);
            dst.put_u16(msg_ref.len() as u16);
            dst.put(msg_ref);

            Ok(())
        }
    }

    impl Decoder for ClientCodec {
        type Item = Response;
        type Error = anyhow::Error;

        fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
            let size = {
                if src.len() < 2 {
                    return Ok(None);
                }
                BigEndian::read_u16(src.as_ref()) as usize
            };

            if src.len() >= size + 2 {
                let _ = src.split_to(2);
                let buf = src.split_to(size);
                Ok(Some(serde_json::from_slice::<Response>(&buf)?))
            } else {
                Ok(None)
            }
        }
    }
```

# How to test 
1. Run a `server`
2. Run `clients` in different `terminals`
3. Enable `tmux sync-panes` to run multple commands of clients in the same time

# TODO
- Missing database support (redis) for recovery of orders
- Add config to have dynamic argument like port, addr, table number and item number
- Gracefully termination of server

# Known issues
- (Fixed) ~~Log's timestamp isn't local time but utc time~~


