use futures::stream::StreamExt;
use futures::SinkExt;
use protocol::codec::ServerCodec;
use protocol::{Request, Response, Table};
use std::ops::ControlFlow;
use std::sync::Arc;
use tables::TableService;
use tokio::net::TcpListener;
use tokio::net::TcpStream;
use tokio::sync::mpsc::unbounded_channel;
use tokio::sync::mpsc::UnboundedSender;
use tracing::*;

mod tables;

const MAX_TABLE_NUM: usize = 1000;
const IDLE_TIME: u64 = 60;

fn process_msg(
    msg: Option<Result<Request, anyhow::Error>>,
    tables: &[UnboundedSender<tables::Request>],
    tx: UnboundedSender<Response>,
) -> Result<ControlFlow<String>, anyhow::Error> {
    match msg {
        Some(Ok(req)) => {
            // check table no.

            let (table, table_req) = match req {
                Request::Ping => return Ok(ControlFlow::Continue(())),
                Request::Order(Table(table), inner) => (table, tables::Request::Order(tx, inner)),
                Request::Delete(Table(table), inner) => (table, tables::Request::Delete(tx, inner)),
                Request::SnapshotAll(Table(table)) => (table, tables::Request::SnapshotAll(tx)),
                Request::Snapshot(Table(table), inner) => {
                    (table, tables::Request::Snapshot(tx, inner))
                }
            };

            anyhow::ensure!(table < MAX_TABLE_NUM, "incorrect table no. {:?} ", table);
            // fire request to corresponding table
            tables[table].send(table_req)?;

            Ok(ControlFlow::Continue(()))
        }
        Some(Err(err)) => Err(err),
        None => Ok(ControlFlow::Break("end of connection".to_string())),
    }
}

#[instrument(name = "peer", skip(socket, table))]
async fn process_socket(
    socket: TcpStream,
    table: &[UnboundedSender<tables::Request>],
    addr: std::net::SocketAddr,
) -> Result<(), anyhow::Error> {
    //let mut frame = tokio_util::codec::Framed::new(socket, tokio_util::codec::LinesCodec::new());
    let mut frame = tokio_util::codec::Framed::new(socket, ServerCodec);
    let (tx, mut rx) = unbounded_channel::<Response>();

    // send an ack to peer about how many tables services we have
    frame
        .send(Response::Welcome {
            table_size: MAX_TABLE_NUM,
        })
        .await?;

    // start listening
    loop {
        tokio::select! {
            // received request from peer
            msg = frame.next() => {
                match process_msg(msg, table, tx.clone()) {
                    Ok(ControlFlow::Continue(_)) => (),
                    Ok(ControlFlow::Break(ret)) => {
                        info!("{}", ret);
                        break
                    }
                    Err(err) => error!(?err), // swallow error rather that propogating error to caller
                }
            }
            // send response to peer
            response = rx.recv() =>
                frame.send(response.ok_or_else(||anyhow::Error::msg("closed channel"))?).await?,

            _ = tokio::time::sleep(tokio::time::Duration::from_secs(IDLE_TIME)) => {
                info!("idle for 60 seconds and disconnection");
                break;
            }
        }
    }

    Ok(())
}

#[tokio::main]
async fn run() -> Result<(), anyhow::Error> {
    let addr = "127.0.0.1:8080";
    let listener = TcpListener::bind(addr).await?;

    // Requirement: The client MAY limit the number of specific tables in its requests to a finite set (at least 100).
    // Here we use 1000 tables for example
    let tables: Arc<Vec<_>> = (0..MAX_TABLE_NUM)
        .into_iter()
        .map(|i| TableService::new(i).run())
        .collect::<Vec<_>>()
        .into();

    info!("start listening to {:?}", addr);
    loop {
        let (socket, peer_addr) = listener.accept().await?;

        info!("new connection from {:?}", peer_addr);
        let tables = tables.clone();
        tokio::spawn(async move {
            if let Err(err) = process_socket(socket, &tables, peer_addr).await {
                error!(?err);
            }
        });
    }
}

fn main() -> Result<(), anyhow::Error> {
    tracing_subscriber::fmt()
        .with_timer(protocol::time::JST)
        .init();

    run()
}
