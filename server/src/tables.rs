use protocol::{Item, Table};
use std::collections::HashMap;
use std::collections::VecDeque;
use tokio::sync::mpsc::unbounded_channel;
use tokio::sync::mpsc::UnboundedSender;
use tokio::sync::oneshot;
use tracing::*;

pub struct TableService {
    table_no: Table,
    items: HashMap<Item, (VecDeque<OrderOnGoing>, VecDeque<oneshot::Sender<()>>)>,
    history: HashMap<Item, Vec<OrderDone>>,
}

#[derive(Debug, Clone)]
pub enum Request {
    Order(UnboundedSender<protocol::Response>, Vec<Item>),
    Delete(UnboundedSender<protocol::Response>, Item),
    SnapshotAll(UnboundedSender<protocol::Response>),
    Snapshot(UnboundedSender<protocol::Response>, Item),

    Cooked(Item), // internal use
}

#[derive(Clone, Debug)]
pub enum OrderStatus {
    Deleted {
        time: chrono::DateTime<chrono::offset::Local>,
    },
    Delivered {
        time: chrono::DateTime<chrono::offset::Local>,
    },
    CantDeliver {
        time: chrono::DateTime<chrono::offset::Local>,
    },
    OnCooking,
}

impl From<OrderStatus> for protocol::OrderStatus {
    fn from(order_status: OrderStatus) -> Self {
        match order_status {
            OrderStatus::Deleted { time } => protocol::OrderStatus::Deleted { time },
            OrderStatus::Delivered { time } => protocol::OrderStatus::Delivered { time },
            OrderStatus::OnCooking => protocol::OrderStatus::OnCooking,
            OrderStatus::CantDeliver { time } => protocol::OrderStatus::CantDeliver { time },
        }
    }
}

#[derive(Clone, Debug)]
pub struct OrderInfo {
    table: Table,
    item: Item,
    received_time: chrono::DateTime<chrono::offset::Local>,
    cook_time: std::time::Duration,
}

impl From<OrderInfo> for protocol::OrderInfo {
    fn from(
        OrderInfo {
            table,
            item,
            received_time,
            cook_time,
        }: OrderInfo,
    ) -> Self {
        protocol::OrderInfo {
            table,
            item,
            received_time,
            cook_time,
        }
    }
}

#[derive(Clone, Debug)]
pub struct OrderOnGoing {
    order_info: OrderInfo,
    peer_tx: UnboundedSender<protocol::Response>,
}

#[derive(Clone, Debug)]
pub struct OrderDone {
    order_info: OrderInfo,
    status: OrderStatus,
}

impl TableService {
    pub fn new(table_no: usize) -> Self {
        TableService {
            table_no: Table(table_no),
            items: HashMap::new(),
            history: HashMap::new(),
        }
    }

    // a loop for listening
    pub fn run(mut self) -> UnboundedSender<self::Request> {
        let (tx, mut rx) = unbounded_channel::<self::Request>();
        let table_no = self.table_no;
        let ret = tx.clone();
        tokio::spawn(
            async move {
                while let Some(req) = rx.recv().await {
                    match req {
                        Request::Order(peer_tx, v) => {
                            if let Err(err) = self.process_order(peer_tx, tx.clone(), v) {
                                error!("can't processing order, err = {:?}", err);
                            }
                        }
                        Request::Cooked(item) => {
                            if let Err(err) = self.process_cooked(item) {
                                error!("can't processing cooked, err = {:?}", err);
                            }
                        }
                        Request::Delete(tx, item) => {
                            if let Err(err) = self.process_delete(tx, item) {
                                error!("can't delete item, err = {:?}", err);
                            }
                        }
                        Request::SnapshotAll(tx) => {
                            if let Err(err) = self.process_snapshot_all(tx) {
                                error!("can't snapshot, err = {:?}", err);
                            }
                        }
                        Request::Snapshot(tx, item) => {
                            if let Err(err) = self.process_snapshot(tx, item) {
                                error!("can't send response for snapshot, err = {:?}", err);
                            }
                        }
                    }
                }
            }
            .instrument(tracing::info_span!("request", table=?table_no.0)),
        );

        ret
    }

    // O(n), where 'n' is size of vector
    fn process_order(
        &mut self,
        peer_tx: UnboundedSender<protocol::Response>,
        tx: UnboundedSender<self::Request>,
        v: Vec<Item>,
    ) -> Result<(), anyhow::Error> {
        let ret = v
            .into_iter()
            .map(|item| {
                let cook_time = std::time::Duration::new(item.0 as u64, 0); //  A simulation to have different cook time, larger item means more time to cook

                let entry = self.items.entry(item).or_default();
                // created Order and pushed back
                let order_info = OrderInfo {
                    table: self.table_no,
                    item,
                    received_time: chrono::offset::Local::now(),
                    cook_time,
                };
                entry.0.push_back(OrderOnGoing {
                    order_info: order_info.clone(),
                    peer_tx: peer_tx.clone(),
                });

                // in case we may have to cancel it later
                let (cancel_tx, cancel_rx) = oneshot::channel::<()>();
                entry.1.push_back(cancel_tx);

                let tx = tx.clone();
                // start cooking
                tokio::spawn(
                    async move {
                        tokio::select! {
                            _ = cancel_rx => {
                                info!("{:?} canceled before it's cooked", item);
                            }
                            _ =  tokio::time::sleep(cook_time) => { // using sleep to simulate cooking
                                tx.send(self::Request::Cooked(item)).unwrap();
                            }
                        };
                    }
                    .in_current_span(),
                );
                Ok(order_info.into())
            })
            .collect::<Vec<_>>();

        debug!("new orders = {:?}", ret);
        peer_tx.send(protocol::Response::Confirmation(ret))?;
        Ok(())
    }

    // O(1), amortized
    fn process_cooked(&mut self, item: Item) -> Result<(), anyhow::Error> {
        self.items
            .get_mut(&item)
            .ok_or_else(|| anyhow::Error::msg("no such item")) // this should never occur cause it's ordered before it gets cooked
            .and_then(|(v_order, v_cancel)| {
                v_order.pop_front().map_or_else(
                    || Ok(()), // nothing to deliver cause this is a special case where it's canceled but also cooked
                    |OrderOnGoing {
                         order_info,
                         peer_tx,
                     }| {
                        // remove those can't cancel because they're already cooked yet not delivered (in the queue to be processed)
                        while matches!(v_cancel.front(), Some(channel) if channel.is_closed()) {
                            v_cancel.pop_front();
                        }

                        let time = chrono::offset::Local::now();
                        // send delivery message to corresponding peer
                        let r = peer_tx
                            .send(protocol::Response::Delivery {
                                time,
                                order_info: order_info.clone().into(),
                            })
                            .map_err(anyhow::Error::new);

                        // Add one in history
                        let v = self.history.entry(order_info.item).or_default();
                        v.push(OrderDone {
                            order_info,
                            status: r
                                .is_ok()
                                .then(|| OrderStatus::Delivered { time })
                                .unwrap_or(OrderStatus::CantDeliver { time }),
                        });
                        r
                    },
                )
            })
    }

    // O(1), amortized
    fn process_delete(
        &mut self,
        tx: UnboundedSender<protocol::Response>,
        item: Item,
    ) -> Result<(), anyhow::Error> {
        match self.items.get_mut(&item) {
            // no such item
            None => tx
                .send(protocol::Response::NoSuchItemToDelete(item))
                .map_err(anyhow::Error::msg),
            // not such item
            Some((v_order, _)) if v_order.is_empty() => tx
                .send(protocol::Response::NoSuchItemToDelete(item))
                .map_err(anyhow::Error::msg),
            // happy path
            Some((v_order, v_cancel)) => {
                // cancel the most recent one
                if let Some(OrderOnGoing {
                    order_info,
                    peer_tx,
                }) = v_order.pop_back()
                {
                    // Order is cancelable
                    // we try to cancel most recent one to save cost
                    while let Some(channel) = v_cancel.pop_back() {
                        if !channel.is_closed() {
                            let _ = channel.send(());
                            break;
                        }
                    }

                    let time = chrono::offset::Local::now();
                    // add one in history since we're done with this order
                    let v_history = self.history.entry(item).or_default();
                    v_history.push(OrderDone {
                        order_info: order_info.clone(),
                        status: OrderStatus::Deleted { time },
                    });

                    // send response to peer
                    let response = protocol::Response::Deleted {
                        time,
                        order_info: order_info.into(),
                    };
                    // because different peers can cancel same item, we have to notify both
                    // send response to peer that ordered if they're from different peers
                    if !tx.same_channel(&peer_tx) {
                        let _ = peer_tx.send(response.clone()).map_err(anyhow::Error::msg);
                    }
                    // send response to peer that sent delete request
                    tx.send(response).map_err(anyhow::Error::msg)
                } else {
                    unreachable!();
                }
            }
        }
    }

    // O(k*(n+m)), where
    // 'n' is size of a vecdeque of a item in items
    // 'm' is size of a vector for a item in history
    // 'k' is size of items
    fn process_snapshot_all(
        &self,
        tx: UnboundedSender<protocol::Response>,
    ) -> Result<(), anyhow::Error> {
        // collect by items' keys
        let ret = self
            .items
            .keys()
            .map(|item| self.snapshot_item(*item))
            .flatten()
            .collect::<Vec<_>>();

        // notify peer
        tx.send(protocol::Response::SnapshotAll(ret))?;
        Ok(())
    }

    // O(snapshot_item)
    fn process_snapshot(
        &self,
        tx: UnboundedSender<protocol::Response>,
        item: Item,
    ) -> Result<(), anyhow::Error> {
        // collect for one item
        let ret = self.snapshot_item(item);
        // notify peer
        tx.send(protocol::Response::Snapshot(ret))?;

        Ok(())
    }

    // O(n+m), where
    // 'n' is size of vecdeque of one item in items
    // 'm' is size of a vector for one item in history
    // A help function to snapshot for one item. Here we collect all ongoing orders and history orders
    fn snapshot_item(&self, item: Item) -> Vec<(protocol::OrderStatus, protocol::OrderInfo)> {
        // collect on-going orders
        let mut ret = self
            .items
            .get(&item)
            .map(|(v_order, _)| {
                v_order
                    .iter()
                    .map(|OrderOnGoing { order_info, .. }| {
                        (protocol::OrderStatus::OnCooking, order_info.clone().into())
                    })
                    .collect::<Vec<_>>()
            })
            .unwrap_or_default();

        // collect history orders
        let tmp = self
            .history
            .get(&item)
            .map(|v| {
                v.iter()
                    .cloned()
                    .map(|OrderDone { order_info, status }| (status.into(), order_info.into()))
                    .collect::<Vec<_>>()
            })
            .unwrap_or_default();

        // merge ret and history
        ret.extend(tmp);

        ret
    }
}
